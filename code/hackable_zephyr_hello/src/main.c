/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>


void main(void)
{
	struct device* port0 = device_get_binding("GPIO_0");

	/* Set LED pin as output */
    gpio_pin_configure(port0, 17, GPIO_DIR_OUT);

	while (1) {
		// flash  LED
		gpio_pin_write(port0, 17, 0);
		k_sleep(500);
		gpio_pin_write(port0, 17, 1);
		k_sleep(500);

		printk("Hello World! %s\r\n", CONFIG_BOARD);

		k_sleep(2000);
	}
}
